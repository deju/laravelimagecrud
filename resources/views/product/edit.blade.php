@include('product.navbar')
<div class="container">
    <div class="row">
        <div class="card">
            <div class="card-header">
                <h3>Create New Product</h3>
                <a href="/list" class="btn btn-sm btn-primary float-end">Back List</a>
            </div>
            <div class="card-body">
                <form action="/update/{{$product->id}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Product Name</label>
                        <input type="text" name="name" value="{{$product->name}}" class="form-control" >
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Price</label>
                        <input type="text" value="{{$product->price}}" name="price" class="form-control" >
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Photo</label>
                        <input type="file" name="picture" class="form-control">
                        <img src="{{asset('product/'.$product->picture)}}" width="150px" height="120px">
                    </div>

                    <button type="submit" class="btn btn-lg btn-warning">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>
