@include('product.navbar')
<div class="container">
    <div class="row">
        <div class="col col-md-4">
            <h3>Related Producs</h3>
            <ul class="list-group list-group-flush">

                @foreach($prods as $prd)
                    <li class="list-group-item"><a href="/show/{{$prd->id}}"> {{$prd->name}}</a></li>
                @endforeach
            </ul>
        </div>
        <div class="col col-md-8">
            <div class="card-body py-3">
                <div class="card" style="width: 28rem;">
                    <img src="{{asset('product/'.$product->picture)}}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Product Details</h5>
                        {{--                        <p class=4card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>--}}
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Product Name: {{$product->name}}</li>
                        <li class="list-group-item">Price : {{$product->price}}</li>
                        {{--                        <li class="list-group-item"></li>--}}
                    </ul>
                    <div class="card-body">
                        <a href="baripress.com" class="card-link">Card link</a>
                        <form action="/delete/{{$product->id}}" method="post">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                        </form>
                        {{--                        <a href="#" class="card-link">Another link</a>--}}
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
