@include('product.navbar')
<div class="container py-3">
    <div class="row">
        <div class="card">
            <div class="card-header">
                <h3>List of Products</h3>
                <a href="/create" class="btn btn-sm btn-primary float-end">Add New</a>
            </div>
            <div class="card-body">
                <table class="table table-responsive table-bordered table-stripped">
                   <thead>
                    <tr>
                        <th>No</th>
                        <th>Product Image</th>
                        <th>Product Name</th>
                        <th>Product Price</th>
                        <th>Action</th>
                    </tr>
                   </thead>
                    <tbody>
                    @foreach($products as $key=>$product)
                    <tr>
                        <td>{{$key +1}}</td>
                        <td>
                            <img src="{{asset('product/'.$product->picture)}}" width="80px" height="40px">
                        </td>
                        <td>{{$product->name}}</td>
                        <td>{{$product->price}}</td>
                        <td>
                            <a href="/edit/{{$product->id}}" class="btn btn-sm btn-warning">Edit</a>
                           <a href="/show/{{$product->id}}" class="btn btn-sm btn-info">Show</a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

