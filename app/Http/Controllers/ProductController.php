<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $products = Product::all();
        return view('product.list',['products'=>$products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $product = new Product();

        $imageName=time().'.'.$request->picture->extension();
        $request->picture->move(public_path('product'),$imageName);
        $product->name=$request->name;
        $product->price=$request->price;
        $product->picture=$imageName;
        $product->save();
        return redirect('/list')->with('success','Successfullly Added');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $prods=Product::all();
        $product = Product::find($id);
        return view('product.show',['product'=>$product,'prods'=>$prods]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $product=Product::find($id);
        return view('product.edit',['product'=>$product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product =Product::find($id);
        $destination = '/product/'.$product->picture;
        if(File::exists($destination)){
            File::delete($destination);
        }
        $imageName=time().'.'.$request->picture->extension();
        $request->picture->move(public_path('product'),$imageName);
        $product->name=$request->name;
        $product->price=$request->price;
        $product->picture=$imageName;
        $product->save();
        return redirect('/list');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $product=Product::find($id);
        $product->delete();
        return redirect('/list');
    }
}
