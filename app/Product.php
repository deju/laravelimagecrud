<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    // fillable fields
    protected $fillable=[
        'name',
        'price',
        'picture'
    ];
}
